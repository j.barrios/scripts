#!/bin/bash

echo "# Creating SSH config"
F=/tmp/ssh-config-${STACK_ID}.conf
testenv stack show ssh-config $STACK_ID >  $F
echo "#  -> wrote SSH config to $F"

echo ""
echo "# Creating stack symbols file"
F=/tmp/symbols-${STACK_ID}.json
testenv stack show symbols $STACK_ID > $F
echo "#  -> wrote stack symbols to $F"

echo ""
echo "# Creating ansible inventory file"
nhosts=$(cat /tmp/symbols-${STACK_ID}.json | jq -r '.host_ips | .[] ' | wc -l)
export HOST_IPS=$(cat /tmp/symbols-${STACK_ID}.json | jq -r '.host_ips | .[] ' | tail -n $(($nhosts-1)) | tr '\n' ' ' | sed 's/ $/\n/' )
export CTRL_IP=$(cat /tmp/symbols-${STACK_ID}.json | jq -r '.host_ips | .[] ' | head -n 1)

export F="/tmp/inventory-${STACK_ID}.ini"
rm -f $F

echo "[controlplane]" >> $F
echo "controller ansible_host=${CTRL_IP} ansible_user=ubuntu ansible_ssh_private_key_file=/Users/j.barrios/.ssh/id_ed25519_testenv.pub" >> $F

echo "[dataplane]" >> $F
for ip in $HOST_IPS;
do
    echo "$ip ansible_user=ubuntu ansible_ssh_private_key_file=/Users/j.barrios/.ssh/id_ed25519_testenv.pub" >> $F
done
echo "#  -> wrote ansible inventory to $F"

echo ""
echo "Contoller UI will be at: https://${CTRL_IP}/"
echo "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook test-vms-playbook.yaml -i $F --extra-vars \"@vm-values.yaml\""
