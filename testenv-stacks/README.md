# Purpose
This repository contains scripts and Ansible playbooks/roles to spin up Testenv stacks *following the steps outlined in our user-facing documentation (as much as possible)*.

# Pre-requisites
- Testenv CLI installed and configured (TODO: link to documentation)
- Access to the `f5-AWS_7531_PTG_Prj_Blue_PD` AWS account. (TODO: link to documentation)
- The following files in the `files/` directory:
  - NGINX repository certs named `nginx-repo.crt` and `nginx-repo.key`
  - A file named `nginx-repo-certs.json` with the contents of the NGINX repository .crt and .key files from the previous step, as documented in step 2 here: https://docs.nginx.com/nginx-management-suite/nim/how-to/app-protect/setup-waf-config-management/#upload-nginx-app-protect-waf-certificate-and-key
  - A file named `nms-license-form.json` with the NMS license information (TODO: track down where this is documented)

# Creating stacks
There are 4 distinct steps to create a Testenv stack. The steps are similar whether creating a Kubernetes or VM stack:
- Login to AWS with `aws sso login`
- Create the basic Testenv stack; VMs or Kubernetes.
- Collect the Testenv stack information and create configuration files for Ansible, SSH, and Helm.
- Call an Ansible playbook to run through the installation of NGINX products.

## Creating a VM stack with NMS
Login to AWS
`aws sso login`

Create the testenv stack. This command will print the command to use in the next step, including the stack id of the newly-created stack.
`./create-vms.sh`

Query testenv and create local configuration files for Ansible, SSH, and Kubernetes.
`STACK_ID=<stack id> ./configure-vm-stack.sh`

Run an Ansible playbook to install NGINX products


## Creating a Kubernetes stack with NMS
Login to AWS
`aws sso login`

Create the testenv stack. This command will print the command to use in the next step, including the stack id of the newly-created stack.
`./create-k8s.sh`

Query testenv and create local configuration files for Ansible, SSH, and Kubernetes.
`STACK_ID=<stack id> ./configure-k8s-stack.sh`

Run an Ansible playbook to install NGINX products
`ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook test-k8s-playbook.yaml -i /tmp/inventory-<stack id>.ini --extra-vars "@k8s-values.yaml"`
