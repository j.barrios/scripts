#!/bin/bash

testenv stack create vm-cluster \
	--cloud aws \
	--os ubuntu-22.04 \
	--aws-region us-west-2 \
	--public-key ~/.ssh/id_ed25519_testenv.pub \
	--num-hosts 3

export STACK_ID=`testenv stack list --format json | jq -r ".[0] | .id"`
echo "STACK_ID=${STACK_ID} ./configure-vm-stack.sh"
