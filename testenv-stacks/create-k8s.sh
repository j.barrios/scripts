#!/bin/bash

testenv stack create k8s \
--cloud aws --aws-region us-west-2 \
--public-key ~/.ssh/id_ed25519_testenv.pub \
--create-default-storage-class true \
--num-worker-nodes 5

export STACK_ID=`testenv stack list --format json | jq -r ".[0] | .id"`
echo "STACK_ID=${STACK_ID} ./configure-k8s-stack.sh"
