# - name: Create directory for docker image files
#   ansible.builtin.file:
#     path: ~/nms-helm/
#     state: directory
#     mode: '0755'

- name: Copy NMS docker images
  register: cp_images_result
  ansible.builtin.copy:
    src: "{{ docker_images }}"
    dest: ~/nms-helm/

- name: Create directory for docker image files for this release
  ansible.builtin.file:
    path: "~/nms-helm/nms-helm-{{ nms_version }}"
    state: directory
    mode: '0755'

- name: Copy docker image archive to working dir
  command: "cp  ~/nms-helm/nms-helm-{{ nms_version }}.tar.gz ~/nms-helm/nms-helm-{{ nms_version }}"

- name: Uncompress docker images
  command: "gunzip -f ~/nms-helm/nms-helm-{{ nms_version }}/nms-helm-{{ nms_version }}.tar.gz"
  when: cp_images_result is changed

- name: Unpack docker images
  command: "tar oxvf ~/nms-helm/nms-helm-{{ nms_version }}/nms-helm-{{ nms_version }}.tar -C ~/nms-helm/nms-helm-{{ nms_version }}"
  when: cp_images_result is changed

- name: Load docker images from archive
  community.docker.docker_image:
    name: "nms-{{ item }}"
    source: "load"
    tag: "{{ nms_version }}"
    load_path: "~/nms-helm/nms-helm-{{ nms_version }}/nms-{{ item }}-{{ nms_version }}.tar.gz"
  loop:
    - apigw
    - core
    - dpm
    - ingestion
    - integrations
#    - utility
  when: cp_images_result is changed

- name: Tag docker images and push to private docker repository
  vars:
    ctrl_ip: "{{ hostvars['controller']['ansible_default_ipv4']['address'] }}"
  community.docker.docker_image:
    name: "nms-{{ item }}:{{ nms_version }}"
    repository: "{{ ctrl_ip }}:5000/{{ item }}"
    tag: "{{ nms_version }}"
    push: yes
    source: local
  loop:
    - apigw
    - core
    - dpm
    - ingestion
    - integrations
#    - utility

# Helm chart
## Release
- name: Enable NGINX helm repository
  ansible.builtin.command:
    cmd: helm repo add nginx-stable https://helm.nginx.com/stable
  when: nms_helm_chart == 'stable'

- name: Update helm repository information
  ansible.builtin.command:
    cmd: helm repo update
  when: nms_helm_chart == 'stable'


## Development
- name: Copy helm chart
  ansible.builtin.copy:
    src: "{{ nms_helm_chart }}"
    dest: ~/nms-helm/nms-helm-{{ nms_version }}/
  when: not nms_helm_chart == 'stable'

- name: Create directory for helm charts
  ansible.builtin.file:
    path: "~/nms-helm/nms-helm-{{ nms_version }}/"
    state: directory
    mode: '0755'
  when: not nms_helm_chart == 'stable'

- name: Unpack helm chart
  command:
    chdir: "~/nms-helm/nms-helm-{{ nms_version }}"
    cmd: "tar oxvf nms-hybrid-{{ nms_version }}.tgz -C ~/nms-helm/nms-helm-{{ nms_version }}/"
  when: not nms_helm_chart == 'stable'


# - name: Update the NGINX request limits so that we can run performance tests
#   ansible.builtin.replace:
#     path: "~/nms-helm/nms-helm-{{ nms_version }}/nms-hybrid/generated/nms-http.conf"
#     regexp: 'rate=200r/s;'
#     replace: 'rate=10000r/s;'

# - name: Update the NGINX request limits so that we can run performance tests
#   ansible.builtin.replace:
#     path: "~/nms-helm/nms-helm-{{ nms_version }}/nms-hybrid/generated/nms-http.conf"
#     regexp: 'rate=10r/s;'
#     replace: 'rate=10000r/s;'

# - name: Update the NGINX configuration for apigw to handle more worker connections
#   ansible.builtin.replace:
#     path: "~/nms-helm/nms-helm-{{ nms_version }}/nms-hybrid/generated/nginx.conf"
#     regexp: 'worker_connections 1024;'
#     replace: 'worker_connections 4096;'


# TODO: Check for helm deployment and run upgrade instead of install.

- name: Install NMS from helm chart
  vars:
    ctrl_ip: "{{ hostvars['controller']['ansible_default_ipv4']['address'] }}"
    prefix: "{{ 'nms-hybrid.' if nms_helm_chart == 'stable' else '' }}"
    chart_dir: "~/nms-helm/nms-helm-{{ nms_version }}/nms-hybrid"
    helm_command: |
      helm install -n nms \
      --set {{ prefix }}adminPasswordHash=$(openssl passwd -6 'Testenv12#') \
      --set {{ prefix }}core.image.repository={{ ctrl_ip }}:5000/core \
      --set {{ prefix }}core.image.tag={{ nms_version }} \
      --set {{ prefix }}apigw.image.repository={{ ctrl_ip }}:5000/apigw \
      --set {{ prefix }}apigw.image.tag={{ nms_version }} \
      --set {{ prefix }}dpm.image.repository={{ ctrl_ip }}:5000/dpm \
      --set {{ prefix }}dpm.image.tag={{ nms_version }} \
      --set {{ prefix }}ingestion.image.repository={{ ctrl_ip }}:5000/ingestion \
      --set {{ prefix }}ingestion.image.tag={{ nms_version }} \
      --set {{ prefix }}integrations.image.repository={{ ctrl_ip }}:5000/integrations \
      --set {{ prefix }}integrations.image.tag={{ nms_version }} \
      --set {{ prefix }}utility.image.repository={{ ctrl_ip }}:5000/utility \
      --set {{ prefix }}utility.image.tag={{ nms_version }} \
      --set {{ prefix }}apigw.nginxWorkerConnections=4096 \
      --set {{ prefix }}apigw.nginxWorkerRlimitNofile=4096 \
      nms \
      {{ 'nginx-stable/nms' if nms_helm_chart == 'stable' else '.' }} \
      --create-namespace
  register: install_result
  shell:
    chdir: "{{ '~' if nms_helm_chart == 'stable' else chart_dir }}"
    cmd: "{{ helm_command }}"

- name: Print helm command
  ansible.builtin.debug:
    msg: "{{ install_result.cmd }}"
  tags: debug_info

- name: Get IP address of controller endpoint
  register: ctrl_api_ip
  ansible.builtin.shell:
    cmd: kubectl -n nms get service apigw -o json | jq -r ".spec.clusterIP"
