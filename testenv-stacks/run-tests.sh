#!/bin/bash

if [[ -z "${STACK_ID}" ]]; then
    echo "Missing STACK_ID env variable"
    exit 1
fi

pushd ~/src/platform

export TESTRUN_SYMBOLS=/tmp/symbols-${STACK_ID}.json
export TESTRUN_SSHCFG=/tmp/ssh-config-${STACK_ID}.conf
export TESTRUN_SSHKEY=~/.ssh/id_ed25519_testenv.pub

make e2e-test
#ginkgo #--label-filter="integration"
#go test

popd

echo export TESTRUN_SYMBOLS=/tmp/symbols-${STACK_ID}.json
echo export TESTRUN_SSHCFG=/tmp/ssh-config-${STACK_ID}.conf
echo export TESTRUN_SSHKEY=~/.ssh/id_ed25519_testenv.pub
