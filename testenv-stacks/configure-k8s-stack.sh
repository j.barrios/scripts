#!/bin/bash

echo "# Creating SSH config"
F=/tmp/ssh-config-${STACK_ID}.conf
testenv stack show ssh-config $STACK_ID >  $F
echo "#  -> wrote to $F"

echo "# Creating symbols file"
F=/tmp/symbols-${STACK_ID}.json
testenv stack show symbols $STACK_ID > $F
echo "#  -> wrote to $F"

echo "# Creating ansible inventory file"
CONTROLLER_IPS=$(cat /tmp/symbols-${STACK_ID}.json | jq -r '.control_node_ips | .[] ' | tr '\n' ' ' | sed 's/ $/\n/')
NODE_IPS=$(cat /tmp/symbols-${STACK_ID}.json | jq -r '.worker_node_ips | .[] ' | tr '\n' ' ' | sed 's/ $/\n/')
F="/tmp/inventory-${STACK_ID}.ini"
rm -f $F
echo "[controlplane]" >> $F
for ip in $CONTROLLER_IPS;
do
    echo "controller ansible_host=$ip ansible_user=ubuntu ansible_ssh_private_key_file=/Users/j.barrios/.ssh/id_ed25519_testenv.pub" >> $F
    ctrl_ip=$ip
done
echo "[dataplane]" >> $F
for ip in $NODE_IPS;
do
    echo "$ip ansible_user=ubuntu ansible_ssh_private_key_file=/Users/j.barrios/.ssh/id_ed25519_testenv.pub" >> $F
done
echo "#  -> wrote to $F"

echo ""
echo "# rm ~/.testenv-k8s-ssh-config.conf && ln -s /tmp/ssh-config-${STACK_ID}.conf ~/.testenv-k8s-ssh-config.conf"
echo "# rm ~/.testenv-k8s-symbols.json && ln -s /tmp/symbols-${STACK_ID}.json ~/.testenv-k8s-symbols.json"
echo "# Copy controller's .kube/config"
echo "# ssh ubuntu@${ctrl_ip} \"cat ~/.kube/config\" | sed -e 's/[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}:/${ctrl_ip}:/g' > ~/.kube/config"
echo "# ANSIBLE_STDOUT_CALLBACK=debug ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook test-k8s-playbook.yaml -i $F --extra-vars \"@k8s-values.yaml\""
