#!/bin/bash

# testenv stack create vm-cluster \
# 	--cloud aws \
# 	--os ubuntu-22.04 \
# 	--tag ubuntu2204 \
# 	--aws-host-instance-type t3a.small \
# 	--aws-region us-west-2 \
# 	--public-key ~/.ssh/id_ed25519_testenv.pub \
# 	--num-hosts 4

testenv stack create vm-cluster \
	--cloud aws \
	--os redhat-9 \
	--tag redhat-9 \
	--aws-host-instance-type t3a.small \
	--aws-region us-west-2 \
	--public-key ~/.ssh/id_ed25519_testenv.pub \
	--num-hosts 6


testenv stack create vm-cluster \
	--cloud aws \
	--os redhat-8 \
	--tag redhat-8 \
	--aws-host-instance-type t3a.small \
	--aws-region us-west-2 \
	--public-key ~/.ssh/id_ed25519_testenv.pub \
	--num-hosts 2


