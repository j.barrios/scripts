Quick and dirty scripts for testenv stacks and instances to support Jason's AppWorld demo of NGINX One SaaS console as described in https://nginxsoftware.atlassian.net/wiki/spaces/~238506589/pages/2721185872/N1+SaaS+AppWorld+Demo+Environment


- Create 3 testenv stacks; one for each OS type requested with `create-vms.sh`. At this time Jorge created these stacks with his permissions. If someone else takes over this work then these stacks will need to be duplicated under someone else's permissions.


```
+---+--------------------------------------+------------+-------+--------+-------------------+------------+
| # | id                                   | type       | cloud | status | age               | tags       |
+---+--------------------------------------+------------+-------+--------+-------------------+------------+
| 1 | 410a8ac7-80e1-4a7b-8fdf-f08e6d7be6f3 | vm-cluster | aws   | ready  | 11 days, 19:25:47 | ubuntu2204 |
| 2 | 81d8d6a1-f1c8-4b87-bef5-c3c8f0e58192 | vm-cluster | aws   | ready  | 11 days, 19:23:01 | redhat-9   |
| 3 | 09379b58-d884-4b2a-8c1a-827112d20446 | vm-cluster | aws   | ready  | 11 days, 18:18:09 | redhat-8   |
+---+--------------------------------------+------------+-------+--------+-------------------+------------+
```


  - Be sure to edit the SSH key used (`--public-key` in `create-vms.sh` and `ansible_ssh_private_key_file` in the `<os>-values.yaml` files) when the instances are instantiated so that Ansible can access them.

- Create 3 data plane keys in N1.
- Edit `configure-vm-stacks.sh` to add the N1 data plane keys.
- Once the stacks are available run `configure-vm-stack.sh` to create the Ansible inventory files and the `provision-all-stacks.sh` script.

- Run `provision-all-stacks.sh` to update the instances.
  
