#!/bin/bash

# nginxone-team tenant
#key1="FGBgIcWiI80Kv9N0Z9glbkvijkXlyazvbJYv6/U6JrU="
#key2="jqIH0KIwgFltvheDIabU6QAtXaZDqo1zsJPiQRb9ye4="
#key3="F5x92p/N6QOQHBe7o2PIDcMhEyyV9wGZ8UdpBdBOGs0="

# nginx-tenant1 tenant
key1="eynGhIdtduAV0UGUvKhMEvgKq9XjFZq5GzViHBBjQyw="
key2="Iqo496JG5/UAAOPPFfHK9Hou8uvViJRfCA8Dc4qda2U="
key3="2YIVKKoenbEUQ6nsTCwGBaJGjxGtEILmT2XZGo28k6I="


deb125=nginx_1.25.3-1~jammy_amd64.deb
debr27=nginx-plus_27-1-jammy_amd64.deb
debr24=nginx-plus_24-3-focal_amd64.deb
debr31=nginx-plus_31-1-jammy_amd64.deb

rpm118=nginx-1.18.0-1.el8.ngx.x86_64.rpm
rpm125=nginx-1.25.3-1.el9.ngx.x86_64.rpm
rpm125_el8=nginx-1.25.3-1.el8.ngx.x86_64.rpm

rpmr24=nginx-plus-24-2.el8.ngx.x86_64.rpm
rpmr27=nginx-plus-27-2.el9.ngx.x86_64.rpm
rpmr31=nginx-plus-31-1.el9.ngx.x86_64.rpm

UBUNTU2204_STACK_ID=`testenv stack list --format json --tag ubuntu2204 | jq -r ".[0] | .id"`
echo "UBUNTU2204_STACK_ID = ${UBUNTU2204_STACK_ID}"
REDHAT_9_STACK_ID=`testenv stack list --format json --tag redhat-9 | jq -r ".[0] | .id"`
echo "REDHAT_9_STACK_ID = ${REDHAT_9_STACK_ID}"
REDHAT_8_STACK_ID=`testenv stack list --format json --tag redhat-8 | jq -r ".[0] | .id"`
echo "REDHAT_8_STACK_ID = ${REDHAT_8_STACK_ID}"

for id in $UBUNTU2204_STACK_ID $REDHAT_9_STACK_ID $REDHAT_8_STACK_ID;
do
    echo "# Creating ${id} SSH config"
    F=/tmp/ssh-config-${id}.conf
    testenv stack show ssh-config ${id} >  $F
    echo "#  -> wrote SSH config to $F"

    echo ""
    echo "# Creating stack symbols file"
    F=/tmp/symbols-${id}.json
    testenv stack show symbols ${id} > $F
    echo "#  -> wrote stack symbols to $F"


done

# ############################
# #   UBUNTU
# ############################
id=$UBUNTU2204_STACK_ID
echo ""
echo "# Creating ansible inventory files for ${id}"
#hosts=$(cat /tmp/symbols-${id}.json | jq -r '.host_ips | .[] ')

hosts=()
for ip in $(cat /tmp/symbols-${id}.json | jq -r '.host_ips | .[] ');
do
    echo "Adding ${ip}"
    hosts+=(${ip})
done

# INVENTORY
export F="/tmp/inventory-${id}.ini"
rm -f $F
echo "[dataplane]" >> $F
echo "${hosts[0]} os=ubuntu f5_hostname=datacenter1-llm-inference-1    dpkey=${key1} install_nginx=oss nginx_artifact=${deb125} install_old_libssl=no   cert_type=valid  nginx_status=active" >> $F
echo "${hosts[1]} os=ubuntu f5_hostname=datacenter1-hal-core-api-lb    dpkey=${key1} install_nginx=oss nginx_artifact=${deb125} install_old_libssl=no   cert_type=valid  nginx_status=active" >> $F
echo "${hosts[2]} os=ubuntu f5_hostname=datacenter1-hal-core-intent-lb dpkey=${key1} install_nginx=plus nginx_artifact=${debr31} install_old_libssl=no  cert_type=valid  nginx_status=active" >> $F
echo "${hosts[3]} os=ubuntu f5_hostname=datacenter1-llm-inference-2    dpkey=${key1} install_nginx=plus nginx_artifact=${debr31} install_old_libssl=no cert_type=valid  nginx_status=active" >> $F

echo "#  -> wrote ansible inventory to $F"



############################
#   RHEL 9
############################
id=$REDHAT_9_STACK_ID
echo ""
echo "# Creating ansible inventory files for ${id}"
hosts=()
for ip in $(cat /tmp/symbols-${id}.json | jq -r '.host_ips | .[] ');
do
    echo "Adding ${ip}"
    hosts+=(${ip})
done

# INVENTORY
export F="/tmp/inventory-${id}.ini"
rm -f $F
echo "[dataplane]" >> $F
echo "${hosts[0]} os=redhat f5_hostname=datacenter2-hal-core-sensors-1 dpkey=${key2}  install_nginx=oss  nginx_artifact=${rpm125}  cert_type=valid    nginx_status=active" >> $F
echo "${hosts[1]} os=redhat f5_hostname=datacenter2-hal-core-sensors-2 dpkey=${key2}  install_nginx=oss  nginx_artifact=${rpm125}  cert_type=valid nginx_status=active" >> $F
echo "${hosts[2]} os=redhat f5_hostname=datacenter2-hal-core-sensors-3 dpkey=${key2}  install_nginx=plus nginx_artifact=${rpmr31}  cert_type=valid    nginx_status=active" >> $F
echo "${hosts[3]} os=redhat f5_hostname=azure-llm-inference-3    dpkey=${key3}  install_nginx=plus nginx_artifact=${rpmr31}  cert_type=valid    nginx_status=active" >> $F
echo "${hosts[4]} os=redhat f5_hostname=azure-llm-inference-4    dpkey=${key3}  install_nginx=plus nginx_artifact=${rpmr31}  cert_type=valid    nginx_status=active" >> $F
echo "${hosts[5]} os=redhat f5_hostname=azure-nav-ctrl-lb        dpkey=${key3}  install_nginx=plus nginx_artifact=${rpmr31}  cert_type=valid  nginx_status=active" >> $F

echo "#  -> wrote ansible inventory to $F"




############################
#   RHEL 8
############################
id=$REDHAT_8_STACK_ID
echo ""
echo "# Creating ansible inventory files for ${id}"
hosts=()
for ip in $(cat /tmp/symbols-${id}.json | jq -r '.host_ips | .[] ');
do
    echo "Adding ${ip}"
    hosts+=(${ip})
done

# INVENTORY
export F="/tmp/inventory-${id}.ini"
rm -f $F
echo "[dataplane]" >> $F
echo "${hosts[0]} os=redhat f5_hostname=azure-nav-ctrl-1 dpkey=${key3}  install_nginx=oss  nginx_artifact=${rpm125_el8} cert_type=valid nginx_status=active" >> $F
echo "${hosts[1]} os=redhat f5_hostname=azure-nav-ctrl-2 dpkey=${key3}  install_nginx=oss  nginx_artifact=${rpm125_el8} cert_type=valid nginx_status=active" >> $F

echo "#  -> wrote ansible inventory to $F"



F=./provision-all-stacks.sh
rm -f $F
echo "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook provision.yaml -i /tmp/inventory-${UBUNTU2204_STACK_ID}.ini --extra-vars \"@ubuntu2204-values.yaml\"" >> $F
echo "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook provision.yaml -i /tmp/inventory-${REDHAT_9_STACK_ID}.ini --extra-vars \"@redhat-values.yaml\"" >> $F
echo "ANSIBLE_HOST_KEY_CHECKING=False ansible-playbook provision.yaml -i /tmp/inventory-${REDHAT_8_STACK_ID}.ini --extra-vars \"@redhat-values.yaml\"" >> $F
echo "Wrote provisioning commands to $F"

